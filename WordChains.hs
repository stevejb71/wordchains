module WordChains where

import Data.Graph.Inductive (mkGraph, undir)
import Data.Graph.Inductive.Graph (UEdge, LNode, UNode, empty, Node)
import Data.Graph.Inductive.Tree (Gr)
import Data.Graph.Inductive.Query.BFS (esp)
import Data.List (delete, foldl', nub, find)
import Control.Monad (liftM, join)
import Data.Char (toLower)
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Maybe (maybe, fromJust)
import Data.Tuple (swap)

type Word = String
newtype Wildcard = Wildcard String deriving (Eq, Ord)
newtype Dictionary = Dictionary [Word]
type NodeWord = LNode Word

findWordChain :: Dictionary -> Word -> Word -> Maybe [Word]
findWordChain d@(Dictionary ws) = searchGraph nodesMap graph
  where nodesMap = M.fromList (map swap nws)
        graph = buildGraph $ M.elems (removeSingletons $ buildMap nws)
        nws = [0..] `zip` ws
        removeSingletons = M.filter ((> 1) . length)

searchGraph :: M.Map String Node -> Gr String () -> Word -> Word -> Maybe [Word]
searchGraph nodesMap graph start end = do
  startNode <- M.lookup start nodesMap
  endNode <- M.lookup end nodesMap
  let swapMap m = M.fromList $ map swap $ M.toList m
  let wordFromNode n = fromJust $ M.lookup n $ swapMap nodesMap
  return $ map wordFromNode $ esp startNode endNode graph

buildGraph ::[[NodeWord]] -> Gr String ()
buildGraph nws = undir $ mkGraph nodes edges
  where nodes = uniques $ join nws
        edges = nws >>= (allEdges . map fst)
        uniques = S.toList . S.fromList  -- faster than nub

buildMap :: [NodeWord] -> M.Map Wildcard [NodeWord]
buildMap nws = foldl step M.empty (allWildcardsWithWords nws) 
  where step m (wc, w) = M.insert wc (maybe [w] (w:) (M.lookup wc m)) m

allWildcardsWithWords :: [NodeWord] -> [(Wildcard, NodeWord)]
allWildcardsWithWords nws = do
    nw@(_, w) <- nws
    wildcard <- map (makeWildcard w) [0..(length w-1)]
    return (wildcard, nw)
    where makeWildcard as i = Wildcard $ (take i as) ++ ('*' : drop (i+1) as)

allEdges :: [Node] -> [UEdge]
allEdges ns 
  | length ns < 2 = []
  | otherwise = [(head ns, m, ()) | m <- tail ns] ++ allEdges (tail ns)

loadDictionary :: FilePath -> IO Dictionary
loadDictionary filePath = liftM createDictionary (readFile filePath)
  where createDictionary contents = Dictionary $ lines $ (map toLower) contents
