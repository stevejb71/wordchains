module Main where

import WordChains
import Data.Map (toList)
import System.Environment (getArgs)
--import System.IO.Unsafe (unsafePerformIO)

choose (Dictionary ws) = Dictionary $ filter (\w -> length w <= 6) ws

--bigDictionary = unsafePerformIO $ loadDictionary "dictionary.txt"

--mediumDictionary = unsafePerformIO $ loadDictionary "mediumDictionary.txt"

main = do
    args <- getArgs
    d <- loadDictionary "dictionary.txt"
    print $ findWordChain d (head args) (args !! 1) 