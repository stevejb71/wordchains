module PreludeClasses where

import Prelude hiding (length)
import qualified Prelude as P (length)
import qualified Data.Text as T

class Lengthy a where
    length :: a -> Int

instance Lengthy [a] where
    length as = P.length as

instance Lengthy T.Text where
    length s = T.length s

